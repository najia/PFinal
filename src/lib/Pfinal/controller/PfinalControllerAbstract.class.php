<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-5
 * @package project_name.package_name
 */
abstract class Pfinal_Controller_Abstract {
	
	protected $httpRequest;
	
	protected $httpResponse;
	
	protected $interceptors;

	protected $parameters;

	/**
	 * 用于页面渲染
	 * @var unknown_type
	 */
	protected $render;
	
	/**
	 * @return the $httpResponse
	 */
	public function getHttpResponse() {
		return $this->httpResponse;
	}

	/**
	 * @param field_type $httpResponse
	 */
	public function setHttpResponse($httpResponse) {
		$this->httpResponse = $httpResponse;
	}

	public function __construct(PfinalHttpRequest $httpRequest){
		$this->httpRequest = $httpRequest;
	}
	
	public function renderHtml($tpl){
		$this->render = new Pfinal_Render_Smarty();
		$this->render->setView($tpl);
	}
	
	public function renderJson(){
		$this->render = new Pfinal_Render_Json();
	}

	public function render(PfinalView $view){
		$this->render = new Pfinal_Render_Smarty();
		$this->render->setView($view->getView());
	}
	/**
	 * @return the $httpRequest
	 */
	public function getHttpRequest() {
		return $this->httpRequest;
	}

	/**
	 * @return the $interceptors
	 */
	public function getInterceptors() {
		return $this->interceptors;
	}

	/**
	 * @return the $render
	 */
	public function getRender() {
		return $this->render;
	}

	/**
	 * @param PfinalHttpRequest $httpRequest
	 */
	public function setHttpRequest($httpRequest) {
		$this->httpRequest = $httpRequest;
	}

	/**
	 * @param field_type $interceptors
	 */
	public function setInterceptors($interceptors) {
		$this->interceptors = $interceptors;
	}

	/**
	 * @param unknown_type $render
	 */
	public function setRender($render) {
		$this->render = $render;
	}


	public function redirect($url){
		header("Location:".$url);
	}

	public function forward($url){
		Pfinal::restart($url);
	}
	

    /**
     * Gets the value of parameters.
     *
     * @return mixed
     */
    public function getParameters($index=0){
        return $this->parameters[$index];
    }

    /**
     * Sets the value of parameters.
     *
     * @param mixed $parameters the parameters
     *
     * @return self
     */
    public function setParameters($parameters){
        $this->parameters = $parameters;
    }
}

?>