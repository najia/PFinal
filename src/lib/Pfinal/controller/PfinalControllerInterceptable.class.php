<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-4
 * @package Pfinal
 */
interface Pfinal_Controller_Interceptable {
		
	public function addInterceptor(Pfinal_Config_Interceptor $interceptor);

}

?>