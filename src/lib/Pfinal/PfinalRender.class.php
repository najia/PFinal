<?php
/**
 * 
 * @author huangyusheng
 * @since 2014-3-5
 * @package project_name.package_name
 */
class PfinalRender {
	
	protected $kernelConfig;
	
	
	public function __construct(PfinalConfig $kernelConfig){
		$this->kernelConfig = $kernelConfig;
	}

	
	public function factory(){
		$viewType = $this->kernelConfig->getConstant()->getViewType();
		switch ($viewType) {
			case Pfinal_Config_Constant::VIEW_SMARTY:
			return new Pfinal_Render_Smarty();
			break;
			
			default:
				;
			break;
		}
	}
	
	public static function getErrorRender($code,$message){
		return new Pfinal_Render_Error($code,$message); 
	}
	
}

?>