<?php

/**
 *
 * @author yusaint
 *        
 */
class Pfinal_Model_Update extends Pfinal_Model_Statement {
	// TODO - Insert your code here
	// 
	protected $kSet = array();
	
	protected $vSet = array();
	
	
	/**
	 * @return the $kSet
	 */
	public function getKSet() {
		return $this->kSet;
	}

	/**
	 * @return the $vSet
	 */
	public function getVSet() {
		return $this->vSet;
	}

	/**
	 * 底层采用PDO，防止sql注入
	 * @param unknown_type $k
	 * @param unknown_type $v
	 */
	public function set($k,$v){
		if (is_string($v)){
			$v = '\''.$v.'\'';
		}
		$k = '`'.$k.'`';
		array_push($this->kSet, $k);
		array_push($this->vSet, $v);
		return $this;
	}
	
	/**
	 * 
	 * @param unknown_type $data
	 */
	public function setBatch($data){
		if (empty($data)){
			return $this;
		}else{
			foreach ($data as $k=>$v){
				$this->set($k, $v);
			}
		}
		return $this;
	}
	
	/**
	 * 
	 */
	public function save(){
		
	}
}

?>