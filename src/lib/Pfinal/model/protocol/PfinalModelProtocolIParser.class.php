<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
interface Pfinal_Model_Protocol_IParser {
	public function parse(Pfinal_Model_Statement $stm);
}

?>