<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class Pfinal_Model_Selector extends Pfinal_Model_Statement {	

	protected $next=null;
	
	protected $adapter;
	
	protected $pre=null;
	protected $filterStack=array();
	protected $joinStack = array();
	protected $filterSignatureStack=array();
		
	public function __construct(Pfinal_Model_Adapter_Interface $adapter = null){
		$this->adapter = $adapter;
	}
	/**
	 * @return the $adapter
	 */
	public function getAdapter() {
		return $this->adapter;
	}

	/**
	 * @param field_type $adapter
	 */
	public function setAdapter($adapter) {
		$this->adapter = $adapter;
	}

	/**
	 * @return the $next
	 */
	public function getNext() {
		return $this->next;
	}

	/**
	 * @return the $pre
	 */
	public function getPre() {
		return $this->pre;
	}

	/**
	 * @param NULL $next
	 */
	public function setNext($next) {
		$this->next = $next;
	}

	/**
	 * @param NULL $pre
	 */
	public function setPre($pre) {
		$this->pre = $pre;
	}
	
	/**
	 * [getJoinStack description]
	 * @return [type] [description]
	 */
	public function getJoinStack(){
		return $this->joinStack;
	}
	
	/**
	 * [getFilterStack description]
	 * @return [type] [description]
	 */
	public function getFilterStack(){
		return $this->filterStack;
	}

	/**
	 * [addFilter description]
	 * @param BaseInterceptor $interceptor [description]
	 */
	public function addFilter(Pfinal_Interceptor_Interface $interceptor){
		$signature=$interceptor->signature();
		if(!in_array($signature, $this->filterSignatureStack)){
			array_push($this->filterStack, $interceptor);
			array_push($this->filterSignatureStack, $signature);
		}
		return $this;
	}
	/**
	 * 
	 * @param unknown_type $signature
	 * @return boolean
	 */
	public function removeFilter($signature){
		$pos=array_search($signature, $this->filterSignatureStack);
		if($pos===null)
			return false;
		else{
			$target=$this->filterStack[$pos];
			if($target->signature()===$signature){
				unset($this->filterStack[$pos]);
			}else{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * [join description]
	 * @param  Selector $selector [description]
	 * @param  [type]   $joinOp   [description]
	 * @return [type]             [description]
	 */
	public function join(Pfinal_Model_Selector $selector){
		array_push($this->joinStack,$selector);
		return $this;
	}
}

?>