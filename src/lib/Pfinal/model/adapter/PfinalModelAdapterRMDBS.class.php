<?php
/**
 *
 * @author yusaint
 *        
 */
class Pfinal_Model_Adapter_RMDBS implements Pfinal_Model_Adapter_Interface {
	
	protected $parser;
	
	protected $socket;
	
	protected $params;
	/**
	 *
	 * @param IProtocolParser $parser
	 */
	public function __construct(Pfinal_Model_Protocol_IParser $parser=null){
		if(is_null($parser)){
			$parser=new Pfinal_Model_Protocol_RMDBSParser();
		}
		$this->parser=$parser;
	}
	
	/**
	 * 初始化链接
	 * @param unknown_type $params
	 */
	public function init($params){
		$this->params = $params;
	}
	
	protected function beforeQuery(){
		if (is_resource($this->socket))
			return;
		$this->socket = mysql_connect($this->params['host'].':'.$this->params['port'],$this->params['user'],$this->params['passwd']);
		if (!$this->socket){
			throw new Pfinal_Exception_Runtime("can not connect to mysql");
		}
		mysql_select_db($this->params['database'],$this->socket);
		mysql_query('SET NAMES UTF8',$this->socket);
		
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Model_Adapter_Interface::query()
	 */
	public function query(Pfinal_Model_Selector $selector) {
		$this->beforeQuery();
		// TODO Auto-generated method stub
		$sql = $this->parser->parse($selector);
		$result = mysql_query($sql,$this->socket);
		$rets = array();
		if ($result){
			while (($row = mysql_fetch_assoc($result))!=false){
				$rets[] = $row;
			}
		}else{
			throw new Pfinal_Exception_Runtime(mysql_error(),mysql_errno());
		}
		return $rets;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Pfinal_Model_Adapter_Interface::insert()
	 */
	public function insert(Pfinal_Model_Insert $insertor){
		$this->beforeQuery();
		$sql = $this->parser->parse($insertor);
		try{
			$result = mysql_query($sql,$this->socket);
			if ($result){
				return mysql_insert_id($this->socket);
			}else{
				throw new Pfinal_Exception_Runtime(mysql_error($this->socket));
			}
		}catch(Exception $ex){
			throw new Pfinal_Exception_Runtime($ex->getMessage(), $ex->getCode());
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Pfinal_Model_Adapter_Interface::execute()
	 */
	public function execute(Pfinal_Model_Update $updater){
		$this->beforeQuery();
		$sql = $this->parser->parse($updater);
		try{
			$result = mysql_query($sql,$this->socket);
			if ($result){
				return mysql_affected_rows($this->socket);
			}else{
				throw new Pfinal_Exception_Runtime(mysql_error($this->socket));
			}
		}catch(Exception $ex){
			throw new Pfinal_Exception_Runtime($ex->getMessage(), $ex->getCode());
		}
	}
	
	/**
	 * [call description]
	 * @param  Pfinal_Model_DDL $ddl [description]
	 * @return [type]                [description]
	 */
	public function call(Pfinal_Model_DDL $ddl){
		$this->beforeQuery();
		$sql = $this->parser->parse($ddl);
		$result = mysql_query($sql,$this->socket);
		$rets = array();
		if ($result){
			while (($row = mysql_fetch_assoc($result))!=false){
				$rets[] = $row;
			}
		}
		return $rets;
	}
	
	public function RMDBSSignature(){
		return $this->socket;
	}
}

?>