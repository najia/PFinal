<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
abstract class Pfinal_Model_Filter_Abstract {

	/**
	 * 单个selector的解析前拦截
	 * @param Selector $selector
	 */
	abstract public function beforeProtocolParse(Selector &$selector);
	
	/**
	 * 所有join关系完结后拦截，如获取字面、分页等等
	 * @param unknown_type $dataSet
	 * @param SelectorChain $chain
	 */
	abstract public function afterQuery(&$result,SelectorChain &$chain);
	
	
	abstract public function signature();
	
}

?>