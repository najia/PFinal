<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
abstract class Pfinal_Model_Filter_BeforeProtocolParse extends Pfinal_Model_Filter_Abstract {
	/**
	 * 所有join关系完结后拦截，如获取字面、分页等等
	 * @param unknown_type $dataSet
	 * @param SelectorChain $chain
	 */
	public function afterQuery(&$result,Pfinal_Model_SelectorChain &$chain){}
	
}

?>