<?php
/**
 * 
 */
class Pfinal_Render_Json extends Pfinal_Render_Abstract{

	public function render(){
		$value = $this->httpResponse->getPropersMap();
		header('Content-Type: text/plain');
		echo json_encode($value);
	}
}