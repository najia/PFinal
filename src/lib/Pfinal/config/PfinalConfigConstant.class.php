<?php
class Pfinal_Config_Constant {
	
	const MODE_DEV = 1;
	const MODE_PRODUCT = 2;
	const MODE_CLI = 3;
	
	const MODE_INTERFACE = 1;
	const MODE_INNOTATION = 2;
	
	const VIEW_SMARTY = 1;
	
	protected $devMode;
	
	protected $urlSeparator = '-';
	
	protected $viewType;
	
	protected $interceptorMode = self::MODE_INTERFACE;
	
	protected $ormEnable;
	
	protected $socketTimeout = 1;
	
	
	
	
	
	/**
	 * @return the $ormEnable
	 */
	public function getOrmEnable() {
		return $this->ormEnable;
	}

	/**
	 * @param field_type $ormEnable
	 */
	public function setOrmEnable($ormEnable) {
		$this->ormEnable = $ormEnable;
	}

	public function setDevMode($devMode = false){
		$this->devMode = $devMode;
	}
	
	public function setUrlSeparator($separator = '-'){
		$this->urlSeparator = $separator;
	}
	
	public function setViewType($viewType){
		$this->viewType = $viewType;
	}
	
	
	
	/**
	 * @return the $devMode
	 */
	public function getDevMode() {
		return $this->devMode;
	}

	/**
	 * @return the $urlSeparator
	 */
	public function getUrlSeparator() {
		return $this->urlSeparator;
	}

	/**
	 * @return the $viewType
	 */
	public function getViewType() {
		return $this->viewType;
	}
	
	

	/**
	 * @return the $interceptorMode
	 */
	public function getInterceptorMode() {
		return $this->interceptorMode;
	}

	/**
	 * @param field_type $interceptorMode
	 */
	public function setInterceptorMode($interceptorMode) {
		$this->interceptorMode = $interceptorMode;
	}
	
	

	/**
	 * @return the $socketTimeout
	 */
	public function getSocketTimeout() {
		return $this->socketTimeout;
	}

	/**
	 * @param number $socketTimeout
	 */
	public function setSocketTimeout($socketTimeout) {
		$this->socketTimeout = $socketTimeout;
	}

	public function run(){
	}
}

?>