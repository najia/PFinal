<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class Pfinal_Invocation_Handler {
	
	protected $handler;
	
	protected $instance;

	protected $method;
	
	
	public function __construct(Pfinal_Controller_Abstract $instance,$method,PfinalConfig $kernelConfig){
		try{
			$this->handler = new ReflectionMethod($instance, $method);
			$this->instance = $instance;
			$this->method = $method;
		}catch(ReflectionException $ex){
			print_r($ex);
			throw new Pfinal_Exception_Notfound();
		}
	}
	
	
	/**
	 * @return the $handler
	 */
	public function getHandler() {
		return $this->handler;
	}

	/**
	 * @return Pfinal_Controller_Abstract
	 */
	public function getInstance() {
		return $this->instance;
	}

	public function getMethod(){
		return $this->method;
	}

	/**
	 * 在这里依次解析interceptor，plugin并且注入到当前action方法的调用中
	 * @param unknown_type $instance
	 * @param unknown_type $method
	 */
	public function invoke(){	
		$this->handler->invoke($this->instance);
		$render = $this->instance->getRender();
		if (null!==$render) {
			$render->setHttpResponse($this->instance->getHttpResponse());
		}
	}
}

?>