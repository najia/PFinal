<?php
/**
 * 
 * @author huangyusheng
 * @since 2014-3-6
 * @package project_name.package_name
 */
class PfinalHttpResponse {
	// TODO - Insert your code here
	protected $propersMap = array();
	
	public function set($key,$value){
		$this->propersMap[$key] = $value;
	}
	/**
	 * @return the $propersMap
	 */
	public function getPropersMap() {
		return $this->propersMap;
	}

	/**
	 * @param multitype: $propersMap
	 */
	public function setPropersMap($propersMap) {
		$this->propersMap = $propersMap;
	}

	
	
}

?>