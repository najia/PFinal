<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class PfinalDispatcher {
	
	protected $kernelConfig;
	
	protected $router;
	
	public function __construct(PfinalConfig $kernelConfig,Pfinal_Route_Abstract $router){
		$this->kernelConfig = $kernelConfig;
		$this->router = $router;
	}
	/**
	 * dispatch部分要实现不同于一般框架的predispatch和postDispatch等功能
	 */
	public function loop(){
		
		$controller = $this->router->getControllerInstance();
		$action = $this->router->getActionKey();
		$interceptorBuilder = new Pfinal_Interceptor_Builder($controller, $action);
		$interceptors = $interceptorBuilder->getInterceptors($this->kernelConfig);
		$controller->setInterceptors($interceptors);
		$invocationHandler = new Pfinal_Invocation_Handler($controller,$action,$this->kernelConfig);
		return $invocationHandler;
	}
}

?>