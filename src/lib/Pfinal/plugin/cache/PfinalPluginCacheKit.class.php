<?php
/**
 * 系统缓存插件，用来增强系统性能，系统默认的cachePlugin是memcache的
 * 统一的缓存工具
 * @author yusaint
 * @since 2014-3-21
 * @project Pfinal
 */
class Pfinal_Plugin_Cache_Kit implements Pfinal_Plugin_Interface {
	
	protected static $handlers;
	
	/**
	 * 用来添加缓存工具，如redis或者memcache，或者file
	 * @param unknown_type $name
	 * @param Pfinal_Plugin_Cache_Interface $handler
	 */
	public static function addHandler($name,Pfinal_Plugin_Cache_Interface $handler){
		self::$handlers[$name] = $handler;
	}
	
	public static function getByName($name){
		if (!isset(self::$handlers[$name])){
			//throw new Pfinal_Exception_Runtime("{$name} is not a valid cache plugin", -1);
		}
		return self::$handlers[$name];
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::start()
	 */
	public function start() {
		
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::stop()
	 */
	public function stop() {
		// TODO Auto-generated method stub
	}
	
	// TODO - Insert your code here
}

?>