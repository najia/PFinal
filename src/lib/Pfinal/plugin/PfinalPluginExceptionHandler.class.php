<?php

/** 
 * @author yusaint
 * 
 */
require_once 'Pfinal/PfinalRender.class.php';
class Pfinal_Plugin_ExceptionHandler implements Pfinal_Plugin_Interface {
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::start()
	 */
	public function start() {
		// TODO Auto-generated method stub
		set_exception_handler(array($this,'handler'));
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::stop()
	 */
	public function stop() {
		// TODO Auto-generated method stub
	}
	
	public function handler($exception){
		if ($exception instanceof Pfinal_Exception_Notfound) {
			PfinalRender::getErrorRender(404, $exception->getMessage())->render();
		}
		elseif ($exception instanceof Pfinal_Exception_Runtime){			
			PfinalRender::getErrorRender(500, $exception->getMessage())->render();
		}
		elseif ($exception instanceof Pfinal_Exception_PermissionDeny){
			PfinalRender::getErrorRender(500, $exception->getMessage())->render();
		}else{
			print_r($exception);
		}
	}
	
	// TODO - Insert your code here
}

?>