<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
abstract class Pfinal_Route_Abstract{
	
	protected $controllerKey = '';
	
	protected $actionKey = '';
	
	protected $controllerInstance = null;
	
	protected $parameters;
	
	/**
	 * @return the $parameters
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * @param field_type $parameters
	 */
	public function setParameters($parameters) {
		$this->parameters = $parameters;
	}

	/**
	 * @return the $controllerKey
	 */
	public function getControllerKey() {
		return $this->controllerKey;
	}

	/**
	 * @return the $actionKey
	 */
	public function getActionKey() {
		return $this->actionKey;
	}

	/**
	 * @param field_type $controllerKey
	 */
	public function setControllerKey($controllerKey) {
		$this->controllerKey = $controllerKey;
	}

	/**
	 * @param field_type $actionKey
	 */
	public function setActionKey($actionKey) {
		$this->actionKey = $actionKey;
	}
	
	
	
	/**
	 * @return the $controllerInstance
	 */
	public function getControllerInstance() {
		return $this->controllerInstance;
	}

	/**
	 * @param NULL $controllerInstance
	 */
	public function setControllerInstance($controllerInstance) {
		$this->controllerInstance = $controllerInstance;
	}

	/**
	 * a/b/c/p1-p2p-p3
	 * @param unknown_type $path
	 * @param unknown_type $fileName
	 */
	protected function isController($path,$fileName){
		if (empty($fileName))
			return null;
		$absoluteFilePath = sprintf('%s%s%s.php',$path,DIRECTORY_SEPARATOR,$fileName);
		$instance = null;
		if (is_file($absoluteFilePath)) {
			require_once $absoluteFilePath;
			$class = sprintf('%sController',ucfirst(strtolower($fileName)));
			require_once 'Pfinal/PfinalHttpRequest.class.php';
			$instance = new $class(new PfinalHttpRequest($this->parameters));
		}
		return $instance;
	}

	
	
	
} 
?>