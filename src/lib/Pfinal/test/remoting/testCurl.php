<?php
define('ROOT', dirname(dirname(dirname(__FILE__))));
define('DS', DIRECTORY_SEPARATOR);
define('CONTROLLER', ROOT);
date_default_timezone_set('Asia/Shanghai');

set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR,array(
dirname(ROOT)
)));

require_once ROOT.DS.'Pfinal.class.php';
Pfinal::run();

$config = array(
	'retryTime'=>1,
	'timeout'=>1,
	'key'=>'2d75dd97353b12fa75550d07f1950f23',
	'secret'=>'cart!@#API',
	'servers'=>array(
		array(
			'host'=>'http://cart.api.vip.com/cart/',
			'port'=>80,
			'weight'=>10,
		),
		array(
			'host'=>'http://cart.api.vipshop.com/cart/',
			'port'=>80,
			'weight'=>0,
		),
	)
);

$conf = new Pfinal_Remoting_Config($config);
$curl = new Pfinal_Remoting_Curl($conf);
$params = array(
	'auto_coupon_switch'=>1,
	'customer_abc'=>'B',
	'is_amount'=>1,
	'is_get_last'=>1,
	'trace_id'=>'41385755035244543548',
	'user_id'=>12486118,
	'ver'=>'2.0',
//	'vip_province'=>'104104',
	'warehouse'=>'VIP_NH',
);
print_r($curl->get('getCart', $params));
