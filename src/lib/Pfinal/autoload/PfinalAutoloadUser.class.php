<?php
class Pfinal_Autoload_User implements Pfinal_Autoload_Interface {
	/*
	 * (non-PHPdoc) @see Pfinal_Autoload_Interface::autoload()
	 */
	public function autoload($className) {
		$segments = explode('_', $className);
		$fileName = '';
		foreach ($segments as $segment){
			$fileName .= $segment;
		}
		$fileName .='.class.php';
		
		unset($segments[0]);
		$segments = array_reverse($segments);
		
		$classFile = sprintf('%s%s%s',strtolower(implode(DIRECTORY_SEPARATOR, $segments)),DIRECTORY_SEPARATOR,$fileName);
		$includePath = explode(PATH_SEPARATOR, get_include_path());
		if (!empty($includePath)){
			foreach ($includePath as $path){
				if (is_file($path.DIRECTORY_SEPARATOR.$classFile)){
					require_once $path.DIRECTORY_SEPARATOR.$classFile;
				}
			}
		}
		return false;
	}
}
?>