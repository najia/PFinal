<?php
class WxAccount_Entity extends Pfinal_Plugin_Orm_Entity{

	const STATUS_OK = 1;
	const STATUS_DELETE = 2;

	public $id;

	public $userId;

	public $account;

	public $password;

	public $status;

	public $createTime;
}