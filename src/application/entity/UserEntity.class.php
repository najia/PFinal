<?php

/** 
 * @author yusaint
 * 
 */
class User_Entity extends Pfinal_Plugin_Orm_Entity{
	const STATUS_OK = 1;
	const STATUS_DELETE = 2;
	// TODO - Insert your code here
	public $id;
	public $email;
	public $passwd;	
	public $status;
	public $createTime;
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return the $passwd
	 */
	public function getPasswd() {
		return $this->passwd;
	}

	/**
	 * @return the $status
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @return the $createTime
	 */
	public function getCreateTime() {
		return $this->createTime;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param field_type $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @param field_type $passwd
	 */
	public function setPasswd($passwd) {
		$this->passwd = $passwd;
	}

	/**
	 * @param field_type $status
	 */
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	 * @param field_type $createTime
	 */
	public function setCreateTime($createTime) {
		$this->createTime = $createTime;
	}


	
}

?>