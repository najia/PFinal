<?php
/**
 * 
 */
class WxAccount_Model extends PfinalModelAbstract{

	public function __construct(){
		$this->tableName = 'wx_account';
		$this->databaseName = 'wx';
		parent::__construct();
	}

	/**
	 * [addWxAccount description]
	 * @param [type] $userId   [description]
	 * @param [type] $account  [description]
	 * @param [type] $password [description]
	 */
	public function addWxAccount($userId,$account,$password){


		$id = $this
				->set('userId',$userId)
				->set('account',$account)
				->set('password',$password)
				->set('status',WxAccount_Entity::STATUS_OK)
				->set('createTime',date('Y-m-d H:i:s'))
				->save();
		return $id;
	}

	public function getAll($userId){
		return $this->fetchAll(array('userId=?'=>$userId,'status=?'=>WxAccount_Entity::STATUS_OK));
	}
}