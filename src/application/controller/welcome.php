<?php
/**
 * 
 */
class WelcomeController extends Pfinal_Controller_Abstract{

	protected $userService;

	public function index(){
		echo 'welcome index';
	}

	public function __construct($httpRequest){
		parent::__construct($httpRequest);
		$this->userService = new User_Service();
	}
	
	public function login(){
		$this->httpResponse->set("pageTitle","welcome");
		$this->renderHtml("login.html");
	}


	public function doLogin(){
		$email = $this->httpRequest->request('email');
		$passwd = $this->httpRequest->request('passwd');
		$user = $this->userService->check($email,$passwd);
		if ($user==false) {
			throw new Pfinal_Exception_Runtime("invalid user or password");
		}
		$this->redirect("welcome/dashboard");
		
	}

	public function doRegister(){
		$email = $this->httpRequest->request("email");
		$password1 = $this->httpRequest->request('passwd');
		$password2 = $this->httpRequest->request('passwd2');
		$id = $this->userService->addUser($email, $password1, $password2);
		$this->redirect("welcome/dashboard");
	}


	public function register(){		
		$this->renderHtml("register.html");
	}


	public function dashboard(){
		$wxAccounts = $this->userService->getWxAccountList(1);
		$this->httpResponse->set('wxAccounts',$wxAccounts);
		$this->renderHtml("dashboard.html");
	}
}